/**
 * title: MyUserDetailsService.java
 * copyleft:©2019-2020 gackey. All Rights Reserved.
 */
package com.gackey.medicine.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import com.gackey.medicine.service.user.UserService;

/**
 * @desc verify user's authentication
 * @className MyUserDetailsService
 * @author gackey
 * @date 2020-01-06 06:32
 */
@Component
public class MyUserDetailsService implements UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("user name is ===> {}", username);
        User userDetails = userService.getUserDetails(username);
        return userDetails;
    }

}
