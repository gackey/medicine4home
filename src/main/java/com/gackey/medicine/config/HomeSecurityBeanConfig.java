/**
 * title: HomeSecurityBeanConfig.java
 * copyleft:©2019-2020 gackey. All Rights Reserved.
 */
package com.gackey.medicine.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import com.gackey.medicine.config.handler.HomeLogoutSuccessHandler;
import com.gackey.medicine.constant.bean.SecurityConfigProperties;

/**
 * @desc 安全配置对象
 * @className HomeSecurityBeanConfig
 * @author gackey
 * @date 2020-05-04 22:56
 */
@Configuration
public class HomeSecurityBeanConfig {

    @Autowired
    private SecurityConfigProperties configProperties;

    @Bean
    @ConditionalOnMissingBean(LogoutSuccessHandler.class)
    public LogoutSuccessHandler logoutSuccessHandler() {
        return new HomeLogoutSuccessHandler(configProperties.getBrowser().getLogoutUrl());
    }
}
