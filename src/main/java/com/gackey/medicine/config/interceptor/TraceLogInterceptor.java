/**
 * title: TraceLogInterceptor.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.config.interceptor;

import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * @desc 日志跟踪拦截器，生成traceId跟踪整条调用链
 * @className TraceLogInterceptor
 * @author gackey
 * @date 2021-09-05 19:18
 */
public class TraceLogInterceptor implements HandlerInterceptor {

    private final static String TRACE_ID = "traceId";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
        throws Exception {
        String traceId = UUID.randomUUID().toString().replaceAll("-", "");
        ThreadContext.put(TRACE_ID, traceId);
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
        throws Exception {
        ThreadContext.remove(TRACE_ID);
    }

}
