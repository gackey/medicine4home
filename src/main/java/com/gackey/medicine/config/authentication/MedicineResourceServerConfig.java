/**
 * title: MedicineResourceServerConfig.java copyleft:©2020-2021 gackey. All
 * Rights Reserved.
 */
package com.gackey.medicine.config.authentication;

import com.gackey.medicine.config.handler.HomeLogoutSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import com.gackey.medicine.constant.bean.SecurityConfigProperties;

/**
 * @desc 资源服务器
 * @className MedicineResourceServerConfig
 * @author gackey
 * @date 2020-10-06 12:00
 */
@Configuration
@EnableResourceServer
public class MedicineResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    private SecurityConfigProperties configProperties;
    @Autowired
    private AuthenticationSuccessHandler homeAuthenticationSuccessHandler;
    @Autowired
    private AuthenticationFailureHandler homeAuthenticationFailureHandler;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.formLogin()
            .loginPage("/browser/security/authentication/check")
            .loginProcessingUrl("/authentication/form")
            .successHandler(homeAuthenticationSuccessHandler)
            .failureHandler(homeAuthenticationFailureHandler)
            .and()
            .authorizeRequests()
            .antMatchers("/authentication/check", "/session/invalid", configProperties.getBrowser().getLoginPage())
            .permitAll()
            .anyRequest().authenticated()
            .and()
            .logout().logoutSuccessHandler(new HomeLogoutSuccessHandler(configProperties.getBrowser().getLogoutUrl()))
            .deleteCookies("JSESSIONID").invalidateHttpSession(true)
            .and()
            .csrf().disable();
    }
}
