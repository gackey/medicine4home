/**
 * title: MedicineAuthorizationServerConfig.java 
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.config.authentication;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

/**
 * @desc 认证服务器
 * @className MedicineAuthorizationServerConfig
 * @author gackey
 * @date 2020-10-06 10:36
 */
@Configuration
@EnableAuthorizationServer
public class MedicineAuthorizationServerConfig {

}
