/**
 * title: HomeLogoutSuccessHandler.java
 * copyleft:©2019-2020 gackey. All Rights Reserved.
 */
package com.gackey.medicine.config.handler;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gackey.medicine.model.ResponseObj;

/**
 * @desc 退出成功处理器
 * @className HomeLogoutSuccessHandler
 * @author gackey
 * @date 2020-05-04 22:46
 */
public class HomeLogoutSuccessHandler implements LogoutSuccessHandler {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private String logoutUrl;

    public HomeLogoutSuccessHandler(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
        throws IOException, ServletException {
        log.info("logout success!");
        if (StringUtils.isBlank(logoutUrl)) {
            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().write(objectMapper.writeValueAsString(new ResponseObj("退出成功！")));
        } else {
            response.sendRedirect(logoutUrl);
        }
    }

}
