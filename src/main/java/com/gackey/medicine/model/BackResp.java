/**
 * title: BackResp.java 
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.model;

import org.springframework.http.HttpStatus;

/**
 * @desc 返回对象
 * @className BackResp
 * @author gackey
 * @date 2020-10-11 20:07
 */
public class BackResp<T> {

    private int status = HttpStatus.OK.value();

    private String message = "success";

    private T result;

    public BackResp() {
        super();
    }

    public BackResp(int status, String message) {
        super();
        this.status = status;
        this.message = message;
    }

    public BackResp(T result) {
        super();
        this.result = result;
    }

    public static <T> BackResp<T> ok() {
        return new BackResp<T>();
    }

    public static <T> BackResp<T> fail() {
        return new BackResp<T>(HttpStatus.INTERNAL_SERVER_ERROR.value(), "fail");
    }

    public static <T> BackResp<T> fail(int status) {
        return new BackResp<T>(status, "fail");
    }

    public static <T> BackResp<T> fail(String msg) {
        return new BackResp<T>(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg);
    }

    public static <T> BackResp<T> fail(int status, String msg) {
        return new BackResp<T>(status, msg);
    }

    public static <T> BackResp<T> ok(T data) {
        return new BackResp<T>(data);

    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

}
