/**
 * title: NavigateBar.java copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.model.user;

/**
 * @desc 导航栏
 * @className NavigateBar
 * @author gackey
 * @date 2020-10-11 22:41
 */
public class NavigateBar {

    private String navigation;

    private String navigateName;

    private String url;

    public String getNavigation() {
        return navigation;
    }

    public void setNavigation(String navigation) {
        this.navigation = navigation;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNavigateName() {
        return navigateName;
    }

    public void setNavigateName(String navigateName) {
        this.navigateName = navigateName;
    }

}
