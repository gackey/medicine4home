/**
 * title: GrantedUserAuthority.java
 * copyleft:©2019-2020 gackey. All Rights Reserved.
 */
package com.gackey.medicine.model.user;

import org.springframework.security.core.GrantedAuthority;

/**
 * @desc 用户权限对象
 * @className GrantedUserAuthority
 * @author gackey
 * @date 2020-06-26 23:43
 */
public class GrantedUserAuthority implements GrantedAuthority {

    private static final long serialVersionUID = 1L;

    private String userAuth;

    public GrantedUserAuthority(String userAuth) {
        super();
        this.userAuth = userAuth;
    }

    private String getUserAuth() {
        return userAuth;
    }

    @Override
    public String getAuthority() {
        return getUserAuth();
    }

}
