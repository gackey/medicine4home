/**
 * title: UserInitData.java copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.model.user;

import java.util.List;

/**
 * @desc 用户初始化数据
 * @className UserInitData
 * @author gackey
 * @date 2020-10-11 22:40
 */
public class UserInitData {

    private String username;

    private List<NavigateBar> navigateBar;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<NavigateBar> getNavigateBar() {
        return navigateBar;
    }

    public void setNavigateBar(List<NavigateBar> navigateBar) {
        this.navigateBar = navigateBar;
    }

}
