/**
 * title: RequestErrorInfo.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.model.requestlog;

/**
 * @desc TODO
 * @className RequestErrorInfo
 * @author gackey
 * @date 2021-09-05 00:22
 */
public class RequestErrorInfo {

    private String ip;

    private String url;

    private String httpMethod;

    private String classMethod;

    private Object requestParams;

    private RuntimeException exception;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getClassMethod() {
        return classMethod;
    }

    public void setClassMethod(String classMethod) {
        this.classMethod = classMethod;
    }

    public Object getRequestParams() {
        return requestParams;
    }

    public void setRequestParams(Object requestParams) {
        this.requestParams = requestParams;
    }

    public RuntimeException getException() {
        return exception;
    }

    public void setException(RuntimeException exception) {
        this.exception = exception;
    }

    @Override
    public String toString() {
        return "RequestErrorInfo [ip=" + ip + ", url=" + url + ", httpMethod=" + httpMethod + ", classMethod="
            + classMethod + ", requestParams=" + requestParams + ", exception=" + exception + "]";
    }

}
