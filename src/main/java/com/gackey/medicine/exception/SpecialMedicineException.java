/**
 * title: SpecialMedicineException.java 
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.exception;

/**
 * @desc 需要特出处理的自定义异常
 * @className SpecialMedicineException
 * @author gackey
 * @date 2020-10-13 01:53
 */
public class SpecialMedicineException extends Exception {

    private static final long serialVersionUID = -8754378930922935404L;

    public SpecialMedicineException() {
        super();
    }

    public SpecialMedicineException(String message) {
        super(message);
    }

}
