/**
 * title: GlobalExceptionHandler.java
 *  copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.exception.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.gackey.medicine.exception.MedicineException;
import com.gackey.medicine.model.BackResp;

/**
 * @desc 全局自定义异常处理
 * @className GlobalExceptionHandler
 * @author gackey
 * @date 2020-10-13 01:36
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    private Logger log = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(value = MedicineException.class)
    @SuppressWarnings("rawtypes")
    public BackResp dealGlobalException(MedicineException ex) {
        log.error(ex.getMessage());
        return BackResp.fail(ex.getMessage());
    }
}
