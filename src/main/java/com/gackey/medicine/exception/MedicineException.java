/**
 * title: MedicineException.java 
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.exception;

/**
 * @desc 自定义异常
 * @className MedicineException
 * @author gackey
 * @date 2020-10-13 01:39
 */
public class MedicineException extends Exception {

    private static final long serialVersionUID = -5570018300806479130L;

    public MedicineException() {
        super();
    }

    public MedicineException(String message, Throwable cause) {
        super(message, cause);
    }

    public MedicineException(String message) {
        super(message);
    }

}
