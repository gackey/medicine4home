/**
 * title: SecurityConfig.java
 * copyleft:©2019-2020 gackey. All Rights Reserved.
 */
package com.gackey.medicine.constant;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import com.gackey.medicine.constant.bean.SecurityConfigProperties;
import com.gackey.medicine.constant.bean.StorageConfigProperties;

/**
 * @desc 配置属性对象
 * @className PropertyConfig
 * @author gackey
 * @date 2020-01-24 16:12
 */
@Configuration
@EnableConfigurationProperties({ SecurityConfigProperties.class, StorageConfigProperties.class })
public class PropertyConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
