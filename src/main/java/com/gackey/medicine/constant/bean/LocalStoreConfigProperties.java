/**
 * title: LocalStoreConfigProperties.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.constant.bean;

/**
 * @desc 本地存储配置
 * @className LocalStoreConfigProperties
 * @author gackey
 * @date 2021-04-13 01:08
 */
public class LocalStoreConfigProperties {

    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
