/**
 * title: StorageConfigProperties.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.constant.bean;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @desc 存储属性配置
 * @className StorageConfigProperties
 * @author gackey
 * @date 2021-04-13 01:07
 */
@ConfigurationProperties(prefix = "medicine.storage")
public class StorageConfigProperties {

    LocalStoreConfigProperties local = new LocalStoreConfigProperties();

    public LocalStoreConfigProperties getLocal() {
        return local;
    }

    public void setLocal(LocalStoreConfigProperties local) {
        this.local = local;
    }

}
