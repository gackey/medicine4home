/*
 * title: package-info.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

/**
 * @desc: entity
 * @className: package-info
 * @author: gackey
 * @date: 2019-04-27 20:14
 */
package com.gackey.medicine.entity;