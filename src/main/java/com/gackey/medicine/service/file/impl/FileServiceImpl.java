/**
 * title: FileServiceImpl.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.service.file.impl;

import com.gackey.medicine.constant.bean.StorageConfigProperties;
import com.gackey.medicine.service.file.FileService;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;

/**
 * @author gackey
 * @desc 本地文件存储服务
 * @className FileServiceImpl
 * @date 2021-04-13 01:13
 */
@Service
public class FileServiceImpl implements FileService {

    private final Logger log = LoggerFactory.getLogger(FileServiceImpl.class);

    @Autowired
    private StorageConfigProperties storageProp;

    @Override
    public void upload(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String uploadPath = storageProp.getLocal().getPath();
        response.setCharacterEncoding(CharEncoding.UTF_8);
        Integer schunk = null;
        Integer schunks = null;
        String name = null;
        BufferedOutputStream os = null;
        try {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(1024);
            factory.setRepository(new File(uploadPath));
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setFileSizeMax(2L * 1024L * 1024L * 1024L);
            upload.setSizeMax(10L * 1024L * 1024L * 1024L);
            List<FileItem> items = upload.parseRequest(request);

            for (FileItem item : items) {
                if (item.isFormField()) {
                    if ("chunk".equals(item.getFieldName())) {
                        schunk = Integer.parseInt(item.getString(CharEncoding.UTF_8));
                    }
                    if ("chunks".equals(item.getFieldName())) {
                        schunks = Integer.parseInt(item.getString(CharEncoding.UTF_8));
                    }
                    if ("name".equals(item.getFieldName())) {
                        name = item.getString(CharEncoding.UTF_8);
                    }
                }
            }
            for (FileItem item : items) {
                if (!item.isFormField()) {
                    String tempFileName = name;
                    if (name != null) {
                        if (schunk != null) {
                            tempFileName = schunk + "_" + name;
                        }
                        File tempFile = new File(uploadPath, tempFileName);
                        //断点续传
                        if (!tempFile.exists()) {
                            item.write(tempFile);
                        }
                    }
                }
            }
            //文件合并
            if (schunk != null && schunks != null && schunk == schunks - 1) {
                File file = new File(uploadPath, name);
                os = new BufferedOutputStream(new FileOutputStream(file));
                for (int i = 0; i < schunks; i++) {
                    File extraFile = new File(uploadPath, i + "_" + name);
                    while (!extraFile.exists()) {
                        Thread.sleep(100);
                    }
                    byte[] bytes = FileUtils.readFileToByteArray(extraFile);
                    os.write(bytes);
                    os.flush();
                    extraFile.delete();
                }
                os.flush();
            }
            response.getWriter().write(name + " upload success.");
        } finally {
            if (os != null) {
                os.close();
            }
        }
    }

    @Override
    public void download(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setCharacterEncoding(CharEncoding.UTF_8);
        String downloadPath = storageProp.getLocal().getPath();
        String name = request.getParameter("name");
        File file = new File(downloadPath, name);
        InputStream is = null;
        OutputStream os = null;
        try {
            long fileLength = file.length();
            response.setContentType("application/x-download");
            String fileName = URLEncoder.encode(file.getName(), CharEncoding.UTF_8);
            response.addHeader("Content-Disposition", "attachment;filename=" + fileName);
            response.setHeader("Accept-Range", "bytes");

            response.setHeader("fSize", String.valueOf(fileLength));
            response.setHeader("fName", fileName);

            long pos = 0, last = fileLength - 1, sum = 0;
            if (StringUtils.isNotEmpty(request.getHeader("Range"))) {
                response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
                //Range  bytes=1-1024
                String rangeNum = request.getHeader("Range").replaceAll("bytes=", "");
                String[] rangeArray = rangeNum.split("-");
                if (rangeArray.length == 2) {
                    pos = Long.parseLong(rangeArray[0].trim());
                    last = Long.parseLong(rangeArray[1].trim());
                    if (last > fileLength - 1) {
                        last = fileLength - 1;
                    }
                } else {
                    pos = Long.parseLong(rangeNum.replaceAll("-", "").trim());
                }
            }
            long rangeLength = last - pos + 1;
            String contentRange = new StringBuffer("bytes=").append(pos).append("-").append(last).append("/").append(fileLength).toString();
            response.setHeader("Content-Range", contentRange);
            response.setHeader("Content-Lenght", String.valueOf(rangeLength));

            os = new BufferedOutputStream(response.getOutputStream());
            is = new BufferedInputStream(new FileInputStream(file));
            is.skip(pos);
            byte[] buffer = new byte[1024];
            int lenght = 0;
            while (sum < rangeLength) {
                lenght = is.read(buffer, 0, (rangeLength - sum) <= buffer.length ? (int) (rangeLength - sum) : buffer.length);
                sum += lenght;
                os.write(buffer, 0, lenght);
            }
            log.info("download success.");
        } finally {
            if (is != null) {
                is.close();
            }
            if (os != null) {
                os.close();
            }
        }
    }

}
