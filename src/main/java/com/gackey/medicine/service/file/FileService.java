/**
 * title: FileService.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.service.file;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @desc 本地文件存储服务
 * @className FileService
 * @author gackey
 * @date 2021-04-13 00:58
 */
public interface FileService {

    void upload(HttpServletRequest request, HttpServletResponse response) throws Exception;

    void download(HttpServletRequest request, HttpServletResponse response) throws Exception;
}
