/**
 * title: UserService.java copyleft:©2019-2020 gackey. All Rights Reserved.
 */
package com.gackey.medicine.service.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import com.gackey.medicine.entity.MedicineRole;
import com.gackey.medicine.entity.MedicineRoleSource;
import com.gackey.medicine.entity.MedicineRoleUser;
import com.gackey.medicine.mapper.MedicineRoleMapper;
import com.gackey.medicine.mapper.MedicineRoleSourceMapper;
import com.gackey.medicine.mapper.MedicineRoleUserMapper;
import com.gackey.medicine.model.user.GrantedUserAuthority;
import com.gackey.medicine.model.user.NavigateBar;
import com.gackey.medicine.model.user.UserInitData;

/**
 * @desc 用户service
 * @className UserService
 * @author gackey
 * @date 2020-06-21 20:03
 */
@Service
public class UserService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private MedicineRoleUserMapper medicineRoleUserMapper;

    @Autowired
    private MedicineRoleMapper medicineRoleMapper;

    @Autowired
    private MedicineRoleSourceMapper medicineRoleSourceMapper;

    @Cacheable(cacheNames = "user_detail", key = "'_'+'#p0'")
    public User getUserDetails(String name) {
        if (StringUtils.isEmpty(name)) {
            log.error("username is null");
            throw new UsernameNotFoundException("用户名称为空！");
        }
        MedicineRoleUser user = medicineRoleUserMapper.selectByName(name);
        if (!Optional.ofNullable(user).isPresent()) {
            log.error("user does not exist");
            throw new UsernameNotFoundException("用户不存在！");
        }
        List<GrantedUserAuthority> userAuthorities = getUserAuthorities(user.getId());
        return new User(user.getLoginName(), user.getPassword(), user.isEnabled(), user.isNonExpired(),
            user.isCredentialsNonExpired(), user.isNonLocked(), userAuthorities);
    }

    private List<GrantedUserAuthority> getUserAuthorities(Integer userId) {
        List<GrantedUserAuthority> authSet = new ArrayList<>();
        if (!Optional.ofNullable(userId).isPresent()) {
            return authSet;
        }
        List<MedicineRole> roleList = medicineRoleMapper.selectByUserId(userId);
        if (CollectionUtils.isEmpty(roleList)) {
            return authSet;
        }
        roleList.forEach(role-> authSet.add(new GrantedUserAuthority("ROLE_" + role.getRoleName())));
        return authSet;
    }

    @Cacheable(cacheNames = "user_init_data", key = "'_'+'#p0'")
    public UserInitData getInitData(String username) {
        List<MedicineRole> roleList = medicineRoleMapper.selectByUsername(username);
        if (CollectionUtils.isEmpty(roleList)) {
            log.error("The user of <{}> does not have any permissions", username);
            return null;
        }
        List<MedicineRoleSource> sourceList = medicineRoleSourceMapper.selectByRoleList(roleList);
        if (CollectionUtils.isEmpty(sourceList)) {
            log.error("The user of <{}> does not have any sources", username);
            return null;
        }
        List<NavigateBar> navigateList = sourceList.stream().map(source-> convert2Navigation(source))
            .collect(Collectors.toList());
        UserInitData userInitData = new UserInitData();
        userInitData.setUsername(username);
        userInitData.setNavigateBar(navigateList);
        return userInitData;
    }

    private NavigateBar convert2Navigation(MedicineRoleSource source) {
        NavigateBar bar = new NavigateBar();
        bar.setNavigation(source.getSourceName());
        bar.setNavigateName(source.getSourceValue());
        bar.setUrl(source.getUrl());
        return bar;
    }
}
