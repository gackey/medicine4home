/*
 * title: package-info.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

/**
 * @desc: service
 * @className: package-info
 * @author: gackey
 * @date: 2019-04-27 20:15
 */
package com.gackey.medicine.service;