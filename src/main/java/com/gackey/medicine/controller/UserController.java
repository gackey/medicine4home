/**
 * title: UserController.java 
 * copyleft:©2019-2020 gackey. All Rights Reserved.
 */
package com.gackey.medicine.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.gackey.medicine.model.BackResp;
import com.gackey.medicine.model.user.UserInitData;
import com.gackey.medicine.service.user.UserService;

/**
 * @desc query user info
 * @className UserController
 * @author gackey
 * @date 2020-02-04 01:32
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * @desc 获取当前用户 
     * @name getCurrentUser 
     * @return Object 
     * @throws
     */
    @GetMapping("/me")
    public Object getCurrentUser(Authentication authentication) {
        //        return SecurityContextHolder.getContext().getAuthentication();
        return authentication;
    }

    /**
     * @desc 用户信息初始化 
     * @name getUserInit 
     * @param user 
     * @return Object 
     * @throws
     */
    @GetMapping("/init")
    @ResponseBody
    public BackResp<UserInitData> getUserInit(@AuthenticationPrincipal UserDetails user) {
        String username = user.getUsername();
        UserInitData initData = userService.getInitData(username);
        return BackResp.ok(initData);
    }
}
