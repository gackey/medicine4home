/**
 * title: BrowserSecurityController.java copyleft:©2019-2020 gackey. All Rights
 * Reserved.
 */
package com.gackey.medicine.controller;

import java.io.IOException;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.gackey.medicine.constant.bean.SecurityConfigProperties;
import com.gackey.medicine.model.ResponseObj;

/**
 * @desc 身份校验controller
 * @className BrowserSecurityController
 * @author gackey
 * @date 2020-01-18 23:27
 */
@RestController("/browser/security")
public class BrowserSecurityController {

    private Logger log = LoggerFactory.getLogger(getClass());

    private RedirectStrategy redirectstrategy = new DefaultRedirectStrategy();

    private SecurityConfigProperties configProperties;

    /**
     * @throws IOException 
     * @desc 当需要身份认证时，跳转到该方法 
     * @name requireAuthentication
     * @param req
     * @param resp
     * @return ResponseObj
     */
    @RequestMapping("/authentication/check")
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public ResponseObj requireAuthentication(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (Optional.of(req).isPresent()) {
            // 引发跳转的请求
            String requestURL = req.getRequestURI();
            log.info("The request that raises the jump is {}", requestURL);
            if (StringUtils.endsWithIgnoreCase(requestURL, ".html")) {
                redirectstrategy.sendRedirect(req, resp, configProperties.getBrowser().getLoginPage());
            }
        }
        return new ResponseObj("用户认证失败，需要重新登录！");
    }

}
