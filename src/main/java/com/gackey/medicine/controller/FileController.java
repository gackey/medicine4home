/**
 * title: FileController.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gackey.medicine.service.file.FileService;

/**
 * @desc 上传文件
 * @className FileController
 * @author gackey
 * @date 2021-04-13 00:30
 */
@RestController("/file")
public class FileController {

    Logger log = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FileService fileService;

    @RequestMapping("/upload")
    public void upload(HttpServletRequest request, HttpServletResponse response) {
        try {
            fileService.upload(request, response);
        } catch (Exception e) {
            log.error("upload failed, {}", e);
        }
    }

    @RequestMapping("/download")
    public void download(HttpServletRequest request, HttpServletResponse response) {

    }
}
