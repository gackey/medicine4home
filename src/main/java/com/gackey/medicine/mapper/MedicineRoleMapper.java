package com.gackey.medicine.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.gackey.medicine.entity.MedicineRole;

@Mapper
public interface MedicineRoleMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(MedicineRole record);

    int insertSelective(MedicineRole record);

    MedicineRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MedicineRole record);

    int updateByPrimaryKey(MedicineRole record);

    List<MedicineRole> selectByUserId(Integer userId);

    List<MedicineRole> selectByUsername(String username);
}