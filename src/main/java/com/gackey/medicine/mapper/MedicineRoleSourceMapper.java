package com.gackey.medicine.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.gackey.medicine.entity.MedicineRole;
import com.gackey.medicine.entity.MedicineRoleSource;

@Mapper
public interface MedicineRoleSourceMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(MedicineRoleSource record);

    int insertSelective(MedicineRoleSource record);

    MedicineRoleSource selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MedicineRoleSource record);

    int updateByPrimaryKey(MedicineRoleSource record);

    List<MedicineRoleSource> selectByRoleId(Integer roleId);

    List<MedicineRoleSource> selectByRoleList(List<MedicineRole> list);
}