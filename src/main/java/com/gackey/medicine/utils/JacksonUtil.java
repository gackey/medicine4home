/**
 * title: JacksonUtil.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.medicine.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @desc TODO
 * @className JacksonUtil
 * @author gackey
 * @date 2021-09-05 18:07
 */
public class JacksonUtil {

    private static final ThreadLocal<ObjectMapper> LOCAL_JSON = new ThreadLocal<ObjectMapper>() {

        @Override
        protected ObjectMapper initialValue() {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
            objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
            return objectMapper;
        }
    };

    public static ObjectMapper getJson() {
        return LOCAL_JSON.get();
    }
}
