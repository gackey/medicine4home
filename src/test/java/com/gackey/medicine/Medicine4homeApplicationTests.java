package com.gackey.medicine;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.gackey.medicine.constant.PropertyConfig;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Medicine4homeApplicationTests {

    @Autowired
    private PropertyConfig securityConfig;

    @Test
    public void contextLoads() {
    }

    @Test
    public void checkBCryptPassword() {
        String encode = securityConfig.passwordEncoder().encode("123456");
        System.out.println("password encoder is : " + encode);
    }

}
