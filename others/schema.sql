CREATE TABLE medicine_role_user(
    id SERIAL PRIMARY KEY,
    login_name varchar(32) NOT NULL,
    "password" varchar(256) NOT NULL,
    enabled boolean not null DEFAULT TRUE,
    non_expired boolean not null DEFAULT TRUE,
    credentials_non_expired boolean not null DEFAULT TRUE,
    non_locked boolean not null DEFAULT TRUE,
    create_time timestamp with time zone not null DEFAULT NOW(),
    update_time timestamp with time zone not null DEFAULT NOW()
);
COMMENT ON table medicine_role_user is '账号表';
COMMENT ON column medicine_role_user.id is '主键';
COMMENT ON column medicine_role_user.login_name is '登录名';
COMMENT ON column medicine_role_user."password" is '密码';
COMMENT ON column medicine_role_user.enabled is '状态：true-正常；false-关闭';
COMMENT ON column medicine_role_user.non_expired is '账号是否过期';
COMMENT ON column medicine_role_user.credentials_non_expired is '证书是否过期';
COMMENT ON column medicine_role_user.non_locked is '账号是否锁定';
COMMENT ON column medicine_role_user.create_time is '创建时间';
COMMENT ON column medicine_role_user.update_time is '修改时间';

CREATE TABLE medicine_role(
    id SERIAL PRIMARY KEY,
    role_name VARCHAR(16) not null,
    description VARCHAR(32) default null,
    status boolean not null DEFAULT TRUE,
    parent_id integer,
    create_time timestamp with time zone not null DEFAULT NOW(),
    update_time timestamp with time zone not null DEFAULT NOW()
);
comment on table medicine_role is '角色表';
comment on column medicine_role.id is '角色id';
comment on column medicine_role.role_name is '角色';
comment on column medicine_role.description is '角色描述';
comment on column medicine_role.status is '状态：true-正常；false-关闭';
comment on column medicine_role.parent_id is '上级角色id';
comment on column medicine_role.create_time is '创建时间';
comment on column medicine_role.update_time is '修改时间';

CREATE TABLE medicine_role_source(
    id SERIAL PRIMARY KEY,
    source_name VARCHAR(32) not null,
    url VARCHAR(256) DEFAULT null,
    source_value VARCHAR(256) DEFAULT null,
    status boolean not null DEFAULT TRUE,
    parent_id integer,
    create_time timestamp with time zone not null DEFAULT NOW(),
    update_time timestamp with time zone not null DEFAULT NOW()
);
comment on table medicine_role_source is '角色资源表';
comment on column medicine_role_source.id is '资源id';
comment on column medicine_role_source.source_name is '资源名称';
comment on column medicine_role_source.url is '资源地址';
comment on column medicine_role_source.source_value is '资源值';
comment on column medicine_role_source.status is '状态：true-正常；false-关闭';
comment on column medicine_role_source.parent_id is '上级角色id';
comment on column medicine_role_source.create_time is '创建时间';
comment on column medicine_role_source.update_time is '修改时间';

CREATE TABLE medicine_role_user_relation(
    id SERIAL PRIMARY KEY,
    user_id integer not null,
    role_id integer not null
);
comment on table medicine_role_user_relation is '账户角色关系表';
comment on column medicine_role_user_relation.user_id is '账户id';
comment on column medicine_role_user_relation.role_id is '角色id';

CREATE TABLE medicine_role_source_relation(
    id SERIAL PRIMARY KEY,
    role_id int not null,
    source_id int not null
);
comment on table medicine_role_source_relation is '角色资源关系表';
comment on column medicine_role_source_relation.role_id is '角色id';
comment on column medicine_role_source_relation.source_id is '资源id';

create table drug_info(
    id SERIAL PRIMARY KEY,
    drug_name varchar(64) not null,
    drug_major_name varchar(32),
    drug_type smallint not null default 0,
    brand_id int not null default 0,
    brand_name varchar(32) not null default '',
    retail_flag boolean not null default false,
    sales_specification smallint not null default 1,
    retail_price money not null default 0.00,
    whole_price money not null default 0.00,
    integral_event_flag boolean not null default false,
    integral_proportion numeric(4,2) not null default 1.0,
    "operator" varchar(32) not null default '',
    create_time timestamp with time zone not null DEFAULT NOW(),
    update_time timestamp with time zone not null DEFAULT NOW()
);
comment on table drug_info is '药品信息表';
comment on column drug_info.id is '药品id';
comment on column drug_info.drug_name is '药品名称';
comment on column drug_info.drug_major_name is '药品专业名称';
comment on column drug_info.drug_type is '药品类型';
comment on column drug_info.brand_id is '品牌厂家id';
comment on column drug_info.brand_name is '品牌厂家名称';
comment on column drug_info.retail_flag is '是否拆分出售，默认false-不拆分';
comment on column drug_info.sales_specification is '销售规格,拆分成几份出售';
comment on column drug_info.retail_price is '零售价';
comment on column drug_info.whole_price is '批发价';
comment on column drug_info.integral_event_flag is '是否参加积分活动';
comment on column drug_info.integral_proportion is '积分比例，积分=售价*积分比例';
comment on column drug_info."operator" is '操作人';
comment on column drug_info.create_time is '创建时间';
comment on column drug_info.update_time is '修改时间';

create table medicine_customer (
    id SERIAL PRIMARY KEY,
    customer_id varchar(32) not null,
    "name" varchar(8) not null,
    phone varchar(16) not null,
    sex smallint default -1,
    identity_card varchar(32) default '',
    address varchar(128) default '',
    consume_count money not null default 0,
    pay_points money not null default 0,
    remark varchar(1024) default '',
    "operator" varchar(32) not null default '',
    create_time timestamp with time zone not null DEFAULT NOW(),
    update_time timestamp with time zone not null DEFAULT NOW()
);
comment on table medicine_customer is '顾客信息表';
comment on column medicine_customer.id is '会员id';
comment on column medicine_customer.customer_id is '会员编号';
comment on column medicine_customer."name" is '会员名称';
comment on column medicine_customer.phone is '手机号';
comment on column medicine_customer.sex is '性别，0-女；1-男';
comment on column medicine_customer.identity_card is '身份证';
comment on column medicine_customer.address is '家庭地址';
comment on column medicine_customer.consume_count is '消费总计';
comment on column medicine_customer.pay_points is '购物积分';
comment on column medicine_customer.remark is '备注';
comment on column medicine_customer."operator" is '操作人';
comment on column medicine_customer.create_time is '创建时间';
comment on column medicine_customer.update_time is '修改时间';
